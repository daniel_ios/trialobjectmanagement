//
//  ListObjectVC.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit
import DGPLibrary
import DGPExtensionCore

class ListItemVC: UIViewController, Storyboarded {
    
    //MARK: - Instance properties
    
    public weak var coordinator: MainCoordinator?
    
    public var viewModel: ListItemViewModel!
    
    private var listItemView: ListItemView! {
        guard isViewLoaded else {
            return nil
        }
        return (view as! ListItemView)
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
    private var tableObject: UITableView! {
        return listItemView.tableObjects
    }
    
    //MARK: - Life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    private func setupView() {
        navigationItem.title = NSLocalizedString("Items", comment: "title list items screen")
        
        let addButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addItem))
        let deleteButton = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteItems))
        
        navigationItem.leftBarButtonItem = editButtonItem
        navigationItem.rightBarButtonItems = [addButton, deleteButton]
        
        navigationController?.navigationBar.prefersLargeTitles = false
        listItemView.tableObjects.allowsMultipleSelectionDuringEditing = true
        
        tableObject.dataSource = self
        tableObject.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = NSLocalizedString("Search items by name", comment: "Placholder for search bar items")
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        listItemView.tableObjects.setEditing(editing, animated: true)
    }
    
    private func setupViewModel() {
        viewModel.showAlertClosure = { [weak self] in
            if let message = self?.viewModel.alertMessage {
                self?.view.makeToast(message)
            }
        }
        
        viewModel.reloadTableViewClosure = { [ weak self ] in
            if let deletedRows = self?.viewModel.deleteIndexPaths {
                self?.tableObject.beginUpdates()
                self?.tableObject.deleteRows(at: deletedRows, with: .automatic)
                self?.tableObject.endUpdates()
                self?.viewModel.deleteIndexPaths = nil
            } else {
                self?.listItemView.tableObjects.reloadData()
            }
            
        }
        
        viewModel.editItemClosure = { [weak self] in
            guard let selected = self?.viewModel.selectedItem else {
                return
            }
            self?.coordinator?.showEditItem(with: selected)
        }
        
        viewModel.searchController = searchController
        viewModel.initFetch()
    }
    
    //MARK: - Actions
    
    @objc func addItem() {
        coordinator?.showAddItem()
    }
    
    @objc func deleteItems() {
        viewModel.deleteIndexPaths = tableObject.indexPathsForSelectedRows
    }
}

//MARK: - UITableView DataSource

extension ListItemVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemViewCell.getIdentifier(), for: indexPath) as! ItemViewCell
        cell.viewModel = viewModel.getCellViewModel(at: indexPath)
        return cell
    }
    
    
}

//MARK: - UITableView Delegate

extension ListItemVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            return
        }
        viewModel.selectedIndexPath = indexPath
    }
    
     func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        viewModel.deleteIndexPaths = [indexPath]
    }
    
    func tableView(_ tableView: UITableView, accessoryButtonTappedForRowWith indexPath: IndexPath) {
        viewModel.selectedIndexPath = indexPath
    }
}

//MARK: - UISeachResultsUpdating

extension ListItemVC: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        viewModel.filteredText = searchController.searchBar.text!
    }
    
}
