//
//  SelectItemVC.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 25/2/21.
//

import UIKit

class SelectItemVC: UIViewController, Storyboarded {
    
    //MARK: - Instance properties
    
    var viewModel: SelectItemViewModel!
    weak var coordinator: Coordinator?
    
    var selectItemView: SelectItemView! {
        guard isViewLoaded else {
            return nil
        }
        return (view as! SelectItemView)
    }
    
    private var tableObjects: UITableView! {
        return selectItemView.tableObjects
    }
    
    //MARK: - Life cicle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.defaultSelectedIndexPath().forEach {
            tableObjects.selectRow(at: $0, animated: false, scrollPosition: .none)
        }
        
    }
    
    private func setupView() {
        tableObjects.dataSource = self
        tableObjects.allowsMultipleSelectionDuringEditing = true
        
        navigationItem.title = NSLocalizedString("Select items", comment: "Title for selected items screen")
        tableObjects.setEditing(true, animated: false)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelAction))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneAction))
    }
    
    private func setupViewModel() {
        
        viewModel.reloadTableViewClosure = { [ weak self] in
            self?.tableObjects.reloadData()
        }
        
        viewModel.initFetch()
    }
    
    //MARK: - Action s
    
    @objc func cancelAction() {
        self.dismiss(animated: true)
    }
    
    @objc func doneAction() {
        viewModel.selectedIndexPaths = tableObjects.indexPathsForSelectedRows ?? []
        self.dismiss(animated: true, completion: {
            
        })
    }
}

//MARK: - UITableView DataSource

extension SelectItemVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemViewCell.getIdentifier(), for: indexPath) as! ItemViewCell
        let model = viewModel.getCellViewModel(at: indexPath)
        cell.viewModel = model
        return cell
    }
    
}

