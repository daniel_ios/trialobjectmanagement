//
//  AddEditVC.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit

class AddEditVC: UIViewController, Storyboarded {
    
    //MARK: - Instance properties
    
    public weak var coordinator: MainCoordinator?
    public var viewModel: AddItemViewModel!
    
    private var addEditView: AddEditView! {
        guard isViewLoaded else {
            return nil
        }
        return (view as! AddEditView)
    }
    
    //MARK: - Life cicle
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
    
    private func setupView() {
        addEditView.setup()
        navigationItem.title = NSLocalizedString("Add Item", comment: "Add an Item")
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveItem))
        addEditView.nameTextField.delegate = self
        addEditView.descriptionTextField.delegate = self
        addEditView.tableObjects.dataSource = self
        checkEnabledPublish()
        addEditView.buttonTypeAction.addTarget(self, action: #selector(selectType), for: .touchUpInside)
        addEditView.tableObjects.setHeaderView()
        
        addEditView.tableObjects.addItemClosure = { [weak self] in
            guard let fromModel = self?.viewModel else {
                return
            }
            
            self?.coordinator?.showEditRelation(selectedItems: fromModel.relatedWithItem,
                                                fromItem: (fromModel as? EditItemViewModel)?.getFromItem(),
                                                completion: fromModel.addRelatedItems)
        }
    }
    
    private func setupViewModel() {
        viewModel.configureView(addEditView)
        addObserver(addEditView.nameTextField)
        addObserver(addEditView.descriptionTextField)
        
        viewModel.showAlertClosure = { [weak self] in
            if let message = self?.viewModel.alertMessage {
                self?.view.makeToast(message, position: .bottom)
            }
        }
        
        viewModel.modifiedItemSuccessClosure = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        
        viewModel.updatedType = { [weak self] in
            if let viewModel = self?.viewModel {
                self?.addEditView.updateType(viewModel.typeText)
            }
        }
        
        viewModel.showTypesPickerClosure = { [weak self] in
            if let view = self?.view, let model = self?.viewModel {
                let picker = DGPPickerView(frame: .zero, viewModel: model)
                picker.show(in: view)
            }
        }
        
        viewModel.reloadTableViewClosure = { [weak self] in
            self?.addEditView.tableObjects.reloadData()
        }
        
        viewModel.initFetch()
    }
    
    //MARK: - Actions
    
    @objc func saveItem() {
        viewModel.saveItem()
    }
    
    @objc func selectType() {
        viewModel.isSelectingType = true
    }
    
    //MARK: - Utils
    
    func checkEnabledPublish() {
        isEnabledButton(isValid())
    }
    
    func isEnabledButton(_ enabled: Bool) {
        navigationItem.rightBarButtonItem?.isEnabled = enabled
    }
        
    func isValid() -> Bool {
        return viewModel.isValid()
    }

}

//MARK: - UITextField Delegate

extension AddEditVC: UITextFieldDelegate {
    
    func addObserver(_ textField: UITextField) {
        textField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        textField.autocorrectionType = .no
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        viewModel.updateText(text: textField.text!, tag: textField.tag)
        checkEnabledPublish()
    }
}

//MARK: - UITableView DataSource

extension AddEditVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ItemViewCell.getIdentifier(), for: indexPath) as! ItemViewCell
        cell.viewModel = viewModel.getCellViewModel(at: indexPath)
        cell.selectionStyle = .none
        cell.accessoryType = .none
        return cell
    }
    
    
}
