//
//  AddItemViewModel.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import Foundation

public class AddItemViewModel {
    
    public enum TagTextField: Int {
        case name = 1
        case description = 2
    }
    
    //MARK: - Instance properties
    
    public let dataSource: ItemDataSourceProtocol
    
    fileprivate var types = [Type]()
    
    fileprivate var selectedType: Type! {
        didSet {
            updatedType?()
        }
    }
    
    public var isSelectingType = false {
        didSet {
            if isSelectingType != oldValue && isSelectingType {
                showTypesPickerClosure?()
            }
        }
    }
    
    public var typeText: String {
        return selectedType.title
    }
    
    public var selectedIndex: Int {
        return types.firstIndex(of: selectedType)!
    }
    
    fileprivate var name: String  = ""
    
    fileprivate var description: String = ""
    
    fileprivate(set) var relatedWithItem: [Item] = []
    
    private var relatedCellViewModels = [ItemCellViewModel]() {
        didSet {
            reloadTableViewClosure?()
        }
    }
    
    var alertMessage: String? {
        didSet {
            showAlertClosure?()
        }
    }
    
    var numberOfCells: Int {
        return relatedCellViewModels.count
    }
    
    var modifiedItemSuccessClosure: (() -> Void)?
    var showAlertClosure: (() -> Void)?
    var updatedType: (() -> Void)?
    var showTypesPickerClosure : (() -> Void)?
    var reloadTableViewClosure: (() -> Void)?
    
    //MARK: - Life cicle
    
    public init(dataSource: ItemDataSourceProtocol) {
        self.dataSource = dataSource
    }
    
    public func initFetch() {
        types = dataSource.getAllTypes()
        selectedType = dataSource.getDefaultType()!
    }
    
    func isValid() -> Bool {
        return !name.isEmpty
    }
    
    public func configureView(_ itemView: AddEditView) {
        itemView.nameTextField.tag = TagTextField.name.rawValue
        itemView.descriptionTextField.tag = TagTextField.description.rawValue
    }
    
    //MARK: - Utils
    
    public func updateText(text: String, tag: Int) {
        switch tag {
        case TagTextField.name.rawValue:
            name = text
        case TagTextField.description.rawValue:
            description = text
        default:
            break
        }
    }
    
    public func saveItem() {
        if let _ = dataSource.createObject(withName: name, description: description, type: selectedType, relatedItems: relatedWithItem) {
            modifiedItemSuccessClosure?()
        } else {
            alertMessage = NSLocalizedString("try_again", comment: "error message something went wrong")
        }
    }
    
    fileprivate func refreshCellViewModel() {
        var models = [ItemCellViewModel]()
        relatedWithItem.forEach {
            models.append(createCellViewModel(with: $0))
        }
        relatedCellViewModels = models
    }
    
    private func createCellViewModel(with item: Item) -> ItemCellViewModel {
        return ItemCellViewModel(name: item.name, description: item.objectDescription ?? "")
    }
    
    public func getCellViewModel(at indexPath: IndexPath) -> ItemCellViewModel {
        return relatedCellViewModels[indexPath.row]
    }
    
    public func addRelatedItems(_ items: [Item]) -> Void {
        relatedWithItem = items
        refreshCellViewModel()
    }
}

//MARK: - EditItemViewModel

class EditItemViewModel: AddItemViewModel {
    
    private let item: Item!
    
    public init(dataSource: ItemDataSourceProtocol, item: Item) {
        self.item = item
        super.init(dataSource: dataSource)
        
        self.name = item.name
        self.description = item.objectDescription ?? ""
    }
    
    public override func initFetch() {
        types = dataSource.getAllTypes()
        selectedType = item.relationType
        relatedWithItem = item.relatedWithItem
        refreshCellViewModel()
    }
    
    public override func configureView(_ itemView: AddEditView) {
        super.configureView(itemView)
        itemView.nameTextField.text = name
        itemView.descriptionTextField.text = description
    }
    
    public override func saveItem() {
        if let _ = dataSource.editItem(withItem: item, name: name, description: description, type: selectedType, relatedItems: relatedWithItem) {
            modifiedItemSuccessClosure?()
        } else {
            alertMessage = NSLocalizedString("try_again", comment: "error message something went wrong")
        }
    }
    
    public func getFromItem() -> Item! {
        return item
    }
}


//MARK: - DGPPickerViewModel Protocol

extension AddItemViewModel: DGPPickerViewModel {
    
    public func numberOfComponents() -> Int {
        return 1
    }
    
    public func numberOfItems() -> Int {
        return types.count
    }
    
    public func titleOfItem(with index: Int, component: Int) -> String {
        return types[index].title
    }
    
    public func didSelectItem(with index: Int, component: Int) -> Void {
        selectedType = types[index]
    }
    
    public func finishDGPPickerView() {
        isSelectingType = false
    }
    
}
