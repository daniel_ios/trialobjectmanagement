//
//  ListObjectViewModel.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import Foundation
import UIKit.UISearchController

public class ListItemViewModel {
    
    //MARK: - Instance properties
    
    let dataSource: ItemDataSourceProtocol
    
    private var listItems : [Item]
    private var filteredItems: [Item] {
        didSet {
            refreshCellViewModel(filtered: true)
            reloadTableViewClosure?()
        }
    }
    
    private var cellViewModels: [ItemCellViewModel] = [] {
        didSet {
            reloadTableViewClosure?()
        }
    }
    
    private var filteredCellViewModels: [ItemCellViewModel] = [] {
        didSet {
            reloadTableViewClosure?()
        }
    }
    
    var numberOfCells: Int {
        return isFiltering ? filteredItems.count : listItems.count
    }
    
    public var filteredText: String = "" {
        didSet {
            if !filteredText.isEmpty {
                filterItems()
            } else if filteredText != oldValue {
                reloadTableViewClosure?()
            }
        }
    }
    
    public weak var searchController: UISearchController?
    
    var isFiltering: Bool {
        return searchController != nil &&
            searchController!.isActive && !filteredText.isEmpty
    }
    
    var alertMessage: String? {
        didSet {
            showAlertClosure?()
        }
    }
    
    var selectedIndexPath: IndexPath? {
        didSet {
            if selectedIndexPath != nil {
                editItemClosure?()
            }
        }
    }
    
    var selectedItem: Item? {
        guard let selectedIndexPath = selectedIndexPath else {
            return nil
        }
        
        if isFiltering {
            return selectedIndexPath.row < filteredItems.count ? filteredItems[selectedIndexPath.row] : nil
        } else {
            return selectedIndexPath.row < listItems.count ? listItems[selectedIndexPath.row] : nil
        }
    }
    
    var deleteIndexPaths: [IndexPath]? {
        didSet {
            deleteSelectedItems()
        }
    }
    
    var reloadTableViewClosure: (() -> Void)?
    var showAlertClosure: (() -> Void)?
    var editItemClosure: (() -> Void)?
    
    
    //MARK: - Life cicle
    
    public init( dataSource: ItemDataSourceProtocol) {
        self.dataSource = dataSource
        listItems = []
        filteredItems = []
    }
    
    public func initFetch() {
        listItems = dataSource.getAllObjects()
        refreshCellViewModel()
    }
    
    //MARK: - Utils
    
    private func refreshCellViewModel(filtered: Bool = false) {
        let items = filtered ? filteredItems : listItems
            var models = [ItemCellViewModel]()
            items.forEach {
                models.append(createCellViewModel(with: $0))
            }
            
            if filtered {
                filteredCellViewModels = models
            } else {
                cellViewModels = models
            }
    }
    
    private func createCellViewModel(with item: Item) -> ItemCellViewModel {
        return ItemCellViewModel(name: item.name, description: item.objectDescription ?? "")
    }
    
    public func getCellViewModel(at indexPath: IndexPath) -> ItemCellViewModel {
        return isFiltering ? filteredCellViewModels[indexPath.row] : cellViewModels[indexPath.row]
    }
    
    private func deleteSelectedItems() {
        guard let deleteIndexPaths = deleteIndexPaths else {
            return
        }
        
        let selectedItems = deleteIndexPaths.map { indexPath in
            return listItems[indexPath.row]
        }
        
        listItems = listItems.filter { item in
            !selectedItems.contains(item)
        }
        
        if dataSource.deleteItems(items: selectedItems) {
            refreshCellViewModel()
        } else {
            alertMessage = NSLocalizedString("Something went wrong, please try again", comment: "try again error")
            initFetch()
        }
    }
    
    public func filterItems() {
        let filtered = listItems.filter { item in
            return item.name.lowercased().contains(filteredText.lowercased())
        }
        self.filteredItems = filtered
    }
    
    
}
