//
//  SelectItemViewModel.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 25/2/21.
//

import Foundation

class SelectItemViewModel {
    
    //MARK: - Instance properties
    
    private var dataSource: ItemDataSourceProtocol
    
    private var selectedItems: [Item]
    private var listItems = [Item]()
    
    private var cellViewModels = [ItemSelectedViewModel]() {
        didSet {
            reloadTableViewClosure?()
        }
    }
    
    public var numberOfCells: Int {
        return cellViewModels.count
    }
    
    public var selectedIndexPaths: [IndexPath]? {
        didSet {
            guard let indexPaths = selectedIndexPaths else {
                return
            }
            
            let items = indexPaths.map {
                return listItems[$0.row]
            }
            completionClosure?(items)
        }
    }
    
    private var fromItem: Item?
    
    public var reloadTableViewClosure: (() -> Void)?
    public var completionClosure: (([Item]) -> Void)?
    
    //MARK: - Life cicle
    
    public init(dataSource: ItemDataSourceProtocol, selectedItems: [Item], fromItem: Item? = nil) {
        self.dataSource = dataSource
        self.selectedItems = selectedItems
        self.fromItem = fromItem
    }
    
    public func initFetch() {
        listItems.append(contentsOf: selectedItems)
        
        var fromItemList = selectedItems
        
        if let fromItem = fromItem {
            fromItemList.append(fromItem)
        }
        
        let notRelated = dataSource.getAllNotRelatedItems(from: fromItemList)
        listItems.append(contentsOf: notRelated)
        refreshCellViewModels()
    }
    
    func refreshCellViewModels() {
        var models = [ItemSelectedViewModel]()
        listItems.forEach {
            models.append(createCellViewModel(with: $0))
        }
        cellViewModels = models
    }
    
    private func createCellViewModel(with item: Item) -> ItemSelectedViewModel {
        let selected = selectedItems.contains(item)
        return ItemSelectedViewModel(name: item.name, description: item.objectDescription ?? "", selected: selected)
    }
    
    public func getCellViewModel(at indexPath: IndexPath) -> ItemSelectedViewModel {
        return cellViewModels[indexPath.row]
    }
    
    public func defaultSelectedIndexPath() -> [IndexPath] {
        return selectedItems.compactMap {
            if let index = listItems.firstIndex(of: $0) {
                return IndexPath(row: index, section: 0)
            }
            return nil
        }
        
    }
}
