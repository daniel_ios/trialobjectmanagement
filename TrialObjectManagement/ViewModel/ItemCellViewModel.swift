//
//  ItemCellViewModel.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import Foundation

protocol BaseItemCellViewModel {
    var name: String { get set }
    var description: String { get set }
}

public struct ItemCellViewModel: BaseItemCellViewModel {
    public var name: String
    public var description: String
}

public class ItemSelectedViewModel: BaseItemCellViewModel  {
    public var name: String
    public var description: String
    public var selected: Bool
    
    public init(name: String, description: String, selected: Bool) {
        self.name = name
        self.description = description
        self.selected = selected
    }
}
