//
//  Type+CoreDataProperties.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//
//

import Foundation
import CoreData


extension Type {

    @nonobjc public class func typeFetchRequest() -> NSFetchRequest<Type> {
        return NSFetchRequest<Type>(entityName: "Type")
    }

    @NSManaged public var title: String
    @NSManaged public var relationObjects: NSSet?

}

// MARK: Generated accessors for relationObjects
extension Type {

    @objc(addRelationObjectsObject:)
    @NSManaged public func addToRelationObjects(_ value: Item)

    @objc(removeRelationObjectsObject:)
    @NSManaged public func removeFromRelationObjects(_ value: Item)

    @objc(addRelationObjects:)
    @NSManaged public func addToRelationObjects(_ values: NSSet)

    @objc(removeRelationObjects:)
    @NSManaged public func removeFromRelationObjects(_ values: NSSet)

}

extension Type : Identifiable {

}
