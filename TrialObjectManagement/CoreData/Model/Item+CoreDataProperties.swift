//
//  Item+CoreDataProperties.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//
//

import Foundation
import CoreData


extension Item {

    @nonobjc public class func itemFetchRequest() -> NSFetchRequest<Item> {
        return NSFetchRequest<Item>(entityName: "Item")
    }

    @NSManaged public var name: String
    @NSManaged public var objectDescription: String?
    @NSManaged public var relationContain: NSSet?
    @NSManaged public var relationType: Type

}

// MARK: Generated accessors for relationContain
extension Item {

    @objc(addRelationContainObject:)
    @NSManaged public func addToRelationContain(_ value: Item)

    @objc(removeRelationContainObject:)
    @NSManaged public func removeFromRelationContain(_ value: Item)

    @objc(addRelationContain:)
    @NSManaged public func addToRelationContain(_ values: NSSet)

    @objc(removeRelationContain:)
    @NSManaged public func removeFromRelationContain(_ values: NSSet)

}

extension Item : Identifiable {

}
