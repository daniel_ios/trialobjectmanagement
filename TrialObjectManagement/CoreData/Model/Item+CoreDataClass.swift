//
//  Item+CoreDataClass.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//
//

import Foundation
import CoreData
import SwiftyJSON

enum ItemAttributes: String {
    case name = "name"
    case objectDescription = "description"
    case type = "type"
}

@objc(Item)
public class Item: NSManagedObject {

    class func entityName() -> String {
        return "Item"
    }
    
    public var relatedWithItem: [Item] {
        let set = relationContain as? Set<Item> ?? []
        return set.sorted {
            $0.name < $1.name
        }
    }
    
    convenience init(name: String, description: String? = nil, type: Type, context: NSManagedObjectContext) {
        self.init(context: context)
        self.name = name
        self.objectDescription = description
        self.relationType = type
    }
    
}

extension Item: Encodable {
    func buildFromJSON(_ json: JSON, type: Type, context: NSManagedObjectContext) {
        if let name = json[ItemAttributes.name.rawValue].string {
            self.name = name
        }
        
        if let description = json[ItemAttributes.objectDescription.rawValue].string {
            self.objectDescription = description
        }
        
        self.relationType = type
    }
}

