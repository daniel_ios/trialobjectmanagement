//
//  Type+CoreDataClass.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//
//

import Foundation
import CoreData
import SwiftyJSON

enum TypeAttributes: String {
    case title = "title"
}

@objc(Type)
public class Type: NSManagedObject {
    
    class func entityName() -> String {
        return "Type"
    }
    
    convenience init(title: String, context: NSManagedObjectContext) {
        self.init(context: context)
        self.title = title
    }
}

extension Type: Encodable {
    func buildFromJSON(_ json: JSON, context: NSManagedObjectContext) {
        if let typeName = json[TypeAttributes.title.rawValue].string {
            self.title = typeName
        }
    }
}

