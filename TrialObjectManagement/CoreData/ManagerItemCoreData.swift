//
//  ManagerItemCoreData.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import Foundation
import CoreData

public protocol ItemDataSourceProtocol {
    func getAllTypes() -> [Type]
    func getAllObjects() -> [Item]
    func getAllNotRelatedItems(from items: [Item]) -> [Item]
    func getDefaultType() -> Type?
    func createObject(withName: String, description: String?, type: Type, relatedItems: [Item]) -> Item?
    func editItem(withItem item: Item, name: String, description: String?, type: Type, relatedItems: [Item]) -> Item?
    func deleteItems(items: [Item]) -> Bool
}

public class ManagerItemCoreData: ItemDataSourceProtocol {
    
    static let sharedInstance = ManagerItemCoreData()
    
    let nameTypes : [String]
    let defaultTypeIndex : Int
    
    private init() {
        nameTypes = [
            "Desk",
            "Computer",
            "Keyboard",
            "Server",
            "Human"
        ]
        defaultTypeIndex = 0
        checkDatabaseInitialized()
    }
    
    public func getAllTypes() -> [Type] {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return []
        }
        
        let request = Type.typeFetchRequest()
        
        guard let result = try? context.fetch(request) else {
            return []
        }
        
        return result
    }
    
    public func getAllObjects() -> [Item] {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return []
        }
        
        let request = Item.itemFetchRequest()
        
        guard let result = try? context.fetch(request) else {
            return []
        }
        
        return result
    }
    
    public func getAllNotRelatedItems(from items: [Item]) -> [Item] {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return []
        }
        
        let request = Item.itemFetchRequest()
        request.predicate = NSPredicate(format: "NOT (self IN %@)", items)
        
        guard let result = try? context.fetch(request) else {
            return []
        }
        
        return result
    }
    
    public func createObject(withName name: String, description: String? = nil, type: Type, relatedItems: [Item]) -> Item? {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return nil
        }
        
        let object = Item(name: name,
                            description: description,
                            type: type,
                            context: context)
        object.addToRelationContain(NSSet(array: relatedItems))
        
        do {
            try ManagerCoreData.sharedInstance.saveContext()
            return object
        } catch {
            return nil
        }
    }
    
    public func editItem(withItem item: Item, name: String, description: String? = nil, type: Type, relatedItems: [Item]) -> Item? {
        item.name = name
        item.objectDescription = description
        item.relationType = type
        
        let oldSelection = item.relatedWithItem
        
        let itemsToDelete = oldSelection.filter {
            !relatedItems.contains($0)
        }
        
        let itemsToAdd = relatedItems.filter {
            !oldSelection.contains($0)
        }
        
        item.removeFromRelationContain(NSSet(array: itemsToDelete))
        item.addToRelationContain(NSSet(array: itemsToAdd))
        
        do {
            try ManagerCoreData.sharedInstance.saveContext()
            return item
        } catch {
            return nil
        }
    }
    
    public func deleteItems(items: [Item]) -> Bool {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return false
        }
        
        items.forEach {
            context.delete($0)
        }
        
        do {
            try ManagerCoreData.sharedInstance.saveContext()
            return true
        } catch {
            return false
        }
    }
    
    public func getDefaultType() -> Type? {
        let name = nameTypes[defaultTypeIndex]
        
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            return nil
        }
        
        let request =  Type.typeFetchRequest()
        request.returnsObjectsAsFaults = false
        request.predicate = NSPredicate(format: "\(TypeAttributes.title.rawValue)=%@", name)
        request.fetchLimit = 1
        if let result = try? context.fetch(request), !result.isEmpty {
            return result[0]
        } else {
            createDefaultTypes()
            return getDefaultType()
        }
    }
    
    private func checkDatabaseInitialized() {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            print("Failed to created initial data")
            fatalError()
        }
        
        let request = Type.typeFetchRequest()
        request.returnsObjectsAsFaults = true
        
        if let result = try? context.fetch(request), result.isEmpty {
            createDefaultTypes()
        }
    }
    
    
    private func createDefaultTypes() {
        guard let context = ManagerCoreData.sharedInstance.managedObjectContext else {
            print("Failed to created initial data")
            fatalError()
        }
        
        nameTypes.forEach {
            let _ = Type(title: $0, context: context)
        }
        
        do {
            try ManagerCoreData.sharedInstance.saveContext()
        } catch {
            print("Failed to created initial data \(error)")
            fatalError()
        }
    }
}
