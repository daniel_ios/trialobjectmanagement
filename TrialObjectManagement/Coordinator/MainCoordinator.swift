//
//  MainCoordinator.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit

class MainCoordinator: NSObject, Coordinator {
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        let vc = ListItemVC.instantiate()
        let viewModel = ListItemViewModel(dataSource: ManagerItemCoreData.sharedInstance)
        vc.viewModel = viewModel
        vc.coordinator = self
        navigationController.delegate = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showAddItem() {
        let vc = AddEditVC.instantiate()
        vc.coordinator = self
        vc.viewModel = AddItemViewModel(dataSource: ManagerItemCoreData.sharedInstance)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showEditItem(with item: Item) {
        let vc = AddEditVC.instantiate()
        vc.coordinator = self
        vc.viewModel = EditItemViewModel(dataSource: ManagerItemCoreData.sharedInstance, item: item)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showEditRelation(selectedItems: [Item],
                          fromItem: Item? = nil,
                          completion: @escaping ([Item]) -> Void) {
        let vc = SelectItemVC.instantiate()
        vc.coordinator = self
        vc.viewModel = SelectItemViewModel(dataSource: ManagerItemCoreData.sharedInstance,
                                           selectedItems: selectedItems,
                                           fromItem: fromItem)
        vc.viewModel.completionClosure = completion
        navigationController.present(UINavigationController(rootViewController: vc), animated: true)
    }
    
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
}

extension MainCoordinator: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController,
                              didShow viewController: UIViewController, animated: Bool) {
        
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }
        
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }
        
        let toViewController = navigationController.transitionCoordinator?.viewController(forKey: .to)

        if let _ = fromViewController as? AddEditVC {
            if let toViewController = toViewController as? ListItemVC  {
                toViewController.viewModel.initFetch()
            }
        }
    }
}
