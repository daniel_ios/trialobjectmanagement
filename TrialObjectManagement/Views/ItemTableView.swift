//
//  ItemTableView.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 25/2/21.
//

import UIKit

class ItemTableView: UITableView {
    
    //MARK: - Instance properties
    
    public var headerHeight: CGFloat {
        return 50
    }
    
    var addItemClosure: (() -> Void)?

    //MARK: - Life cicle
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        let nibItemCell = UINib(nibName: ItemViewCell.getIdentifier(), bundle: nil)
        self.register(nibItemCell, forCellReuseIdentifier: ItemViewCell.getIdentifier())
        tableFooterView = UIView()
    }
    
    public func setHeaderView() {
        let header = UIView()
        let titleLabel = UILabel()
        let buttonAdd = UIButton()
        
        header.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        buttonAdd.translatesAutoresizingMaskIntoConstraints = false
        
        self.tableHeaderView = header
        header.addSubview(titleLabel)
        header.addSubview(buttonAdd)
        
        let sizeButtons: CGFloat = 40
        
        NSLayoutConstraint.activate( [
            header.heightAnchor.constraint(equalToConstant: headerHeight),
            header.widthAnchor.constraint(equalTo: self.widthAnchor),
            titleLabel.leadingAnchor.constraint(equalTo: header.leadingAnchor, constant: 10),
            titleLabel.centerYAnchor.constraint(equalTo: header.centerYAnchor),
            buttonAdd.trailingAnchor.constraint(equalTo: header.trailingAnchor, constant: -10),
            buttonAdd.centerYAnchor.constraint(equalTo: header.centerYAnchor),
            buttonAdd.widthAnchor.constraint(equalToConstant: sizeButtons),
            buttonAdd.heightAnchor.constraint(equalToConstant: sizeButtons),
        ])
        
        titleLabel.text = NSLocalizedString("Relations:", comment: "Header list of relations of items")
        buttonAdd.setImage(UIImage(symbol: .plusCircleFill), for: .normal)
        titleLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        
        self.layoutIfNeeded()
        
        buttonAdd.addTarget(self, action: #selector(addAction), for: .touchUpInside)
        buttonAdd.setImage(UIImage(symbol: .pencilCircleFill), for: .normal)
    }
    
    @objc func addAction() {
        addItemClosure?()
    }
}
