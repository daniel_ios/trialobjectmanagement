//
//  ListObjectView.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit

class ListItemView: UIView {
    
    //MARK: - Outlets
    
    @IBOutlet var tableObjects: ItemTableView!
    
}
