//
//  ItemViewCell.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit

public class ItemViewCell: UITableViewCell {
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
    var viewModel: BaseItemCellViewModel? {
        didSet {
            nameLabel.text = viewModel?.name
            descriptionLabel.text = viewModel?.description
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.font = UIFont.systemFont(ofSize: 15, weight: .semibold)
        descriptionLabel.font = UIFont.systemFont(ofSize: 13, weight: .regular)
    }
    
    public override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: true)
        (viewModel as? ItemSelectedViewModel)?.selected = selected
    }
}
