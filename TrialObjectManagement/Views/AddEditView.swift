//
//  AddEditView.swift
//  TrialObjectManagement
//
//  Created by Daniel Gallego Peralta on 24/2/21.
//

import UIKit

public class AddEditView: UIView {
    
    //MARK: - Outlets
    
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var typeValueLabel: UILabel!
    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var nameTextField: UITextField!
    
    @IBOutlet var descriptionLabel: UILabel!
    @IBOutlet var descriptionTextField: UITextField!
    
    @IBOutlet var buttonTypeAction: UIButton!
    
    @IBOutlet var contentTypeView: UIView!
    @IBOutlet var contentNameView: UIView!
    @IBOutlet var contentDescriptionView: UIView!
    
    @IBOutlet weak var tableObjects: ItemTableView!
    
    public func setup() {
        typeLabel.text = NSLocalizedString("Type", comment: "Type of item")
        nameLabel.text = NSLocalizedString("Name", comment: "Name of the Item")
        descriptionLabel.text = NSLocalizedString("Description", comment: "Description of the item")
        
        nameTextField.placeholder = NSLocalizedString("Insert name of the item", comment: "Placeholder for item's name")
        descriptionTextField.placeholder = NSLocalizedString("Add a description of the item", comment: "placeholder for item's description")
        
        let fontTitle = UIFont.systemFont(ofSize: 14, weight: .semibold)
        let fontValue = UIFont.systemFont(ofSize: 14, weight: .regular)
        
        typeLabel.font = fontTitle
        nameLabel.font = fontTitle
        descriptionLabel.font = fontTitle
        
        typeValueLabel.font = fontValue
        nameTextField.font = fontValue
        descriptionTextField.font = fontValue
        
        self.backgroundColor = .gray_light_dusty
        tableObjects.backgroundColor = .clear
        
        setCornerRadius(contentTypeView)
        setCornerRadius(contentNameView)
        setCornerRadius(contentDescriptionView)
    }
    
    func updateType(_ value: String) {
        typeValueLabel.text = value
    }
    
    private func setCornerRadius(_ view: UIView) {
        view.clipsToBounds = true
        view.layer.cornerRadius = 6
    }
}
