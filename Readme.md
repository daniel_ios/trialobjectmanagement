# README #

App for managing objects. You can create/edit/delete objects and add relations  between them.


### Arquitecture ###

* MVVM
* Coordinator

- MVVM and coordinator are the patterns I normally use, this pattern allows each component to be only responsible for a single responsibility and build an scalable app.

### Libraries and protocols ###

* CoreData: add the persistent storage
* SwiftyJSON and Codable protocol: allow to encode/decode objects from json 
* Internal libraries for reuse components between projects

